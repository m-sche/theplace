<?php

define('BOARD_WIDTH', 64);
define('BOARD_HEIGHT', 72);
define('COOLDOWN', 60);
define('GAME_ID', 2);

require('sqlcfg.php');

//////


if ($_SERVER['REQUEST_METHOD'] === 'GET') {
	if (isset($_GET['since']) && !empty($_GET['since']))
		get_changes($_GET['since']);
	else
		get_board();
} elseif ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['x']) && isset($_POST['y']) && isset($_POST['color'])) {
	draw_pixel($_POST['x'], $_POST['y'], $_POST['color']);
	//die("GAME OVER"); // Set at the end of the game
}


///////


function draw_pixel($x, $y, $color) {
	if ($x < 0 || $y < 0 || $x >= BOARD_WIDTH || $y >= BOARD_HEIGHT) {
		http_response_code(400);
		die("Incorrect dimensions");
	}

	if (empty($_COOKIE['token'])) {
		http_response_code(400);
		die("Missing credentials");
	}
	
	$db = new mysqli(SQL_HOST, SQL_USERNAME, SQL_PASSWORD, SQL_DBNAME);
	if ($db->connect_error) {
		http_response_code(500);
		die($db->connect_error);
	}

	$ip = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
	$user = $_COOKIE['token'];

	// Check user's cooldown
	$stmt = $db->prepare('select now(3) > last_action + interval ' . COOLDOWN . ' second from users where token = ?');
	$stmt->bind_param('s', $user);
	if (!$stmt->execute()) {
		$db->rollback();
		http_response_code(500);
		die("Interval failed");
	}
	$cur = $stmt->get_result();
	$stmt->close();
	$res = $cur->fetch_row();
	if (!empty($res) && $res[0] === 0) {
		http_response_code(400);
		die("You must wait for cooldown");
	}
	$cur->free();

	$db->begin_transaction();

	// Insert user
	$stmt = $db->prepare('insert into users (token, ip, last_action) values(?, ?, now(3)) on duplicate key update last_action = now(3)');
	$stmt->bind_param('ss', $user, $ip);
	if (!$stmt->execute()) {
		$db->rollback();
		http_response_code(500);
		die("User insertion failed");
	}
	$stmt->close();


	// Insert pixel
	$stmt = $db->prepare('insert into board (x, y, color) values (?, ?, ?) on duplicate key update color=?');
	$stmt->bind_param('dddd', $x, $y, $color, $color);
	if (!$stmt->execute()) {
		$db->rollback();
		http_response_code(500);
		die("Pixel insertion failed");
	}
	$stmt->close();


	// Insert event
	$stmt = $db->prepare('insert into events (x, y, color, user, timestamp) values (?, ?, ?, ?, now(3))');
	$stmt->bind_param('ddds', $x, $y, $color, $user);
	if (!$stmt->execute()) {
		$db->rollback();
		http_response_code(500);
		die("Event insertion failed");
	}
	$stmt->close();

	$db->commit();
	$db->close();

	var_dump($_SERVER);	// XXX
}

function get_board() {
	$db = new mysqli(SQL_HOST, SQL_USERNAME, SQL_PASSWORD, SQL_DBNAME);
	if ($db->connect_error) {
		http_response_code(500);
		die($db->connect_error);
	}

	// Periodic deletion of inactive users (doesn't make sense at the moment, because users are referenced in events)
	// $db->query('delete from users where now(3) > last_action + interval ' . COOLDOWN . ' second');

	// Fetch colors
	$colors = [];
	if ($cur = $db->query('select id, hexa, background from color')) {
		while ($color = $cur->fetch_assoc()) {
			$colors[$color['id']] = $color['hexa'];
			if ($color['background'])
				$background = (int)$color['id'];
		}
		$cur->free();
	}

	// Fetch board
	$board = array_fill(0, BOARD_WIDTH * BOARD_HEIGHT, $background);
	$now = null;
	if ($cur = $db->query('select x, y, color, round(unix_timestamp(now(3)) * 1000) as now from board where x < ' . BOARD_WIDTH . ' and y < ' . BOARD_HEIGHT . ' and game = ' . GAME_ID)) {
		while ($point = $cur->fetch_row()) {
			$x = (int)$point[0];
			$y = (int)$point[1];
			$board[$y * BOARD_WIDTH + $x] = (int)$point[2];
			$now = $point[3];
		}
		$cur->free();
	}

	if (empty($_COOKIE['token'])) {
		setcookie('token', bin2hex(random_bytes(8)), time() + 7 * 24 * 3600);
		$remaining = COOLDOWN;
	} else {
		$user = $_COOKIE['token'];

		// Check user's cooldown
		$stmt = $db->prepare('select last_action + interval ' . COOLDOWN . ' second - now(3) from users where token = ?');
		$stmt->bind_param('s', $user);
		if (!$stmt->execute()) {
			$db->rollback();
			http_response_code(500);
			die("Interval failed");
		}
		$cur = $stmt->get_result();
		$stmt->close();
		$res = $cur->fetch_row();
		if (empty($res) || $res[0] < 0)
			$remaining = 0;
		else
			$remaining = floor($res[0]);
		$cur->free();
	}

	$db->close();

	header('Content-Type: application/json');
	echo json_encode([
		'timestamp' => $now,
		'dimensions' => [BOARD_WIDTH, BOARD_HEIGHT],
		'colors' => $colors,
		'background' => $background,
		'cooldown' => COOLDOWN,
		'board' => $board,
		'remaining' => $remaining
	]);
}

function get_changes($since) {
	if (empty($_COOKIE['token'])) {
		http_response_code(400);
		die("Missing credentials");
	}

	$db = new mysqli(SQL_HOST, SQL_USERNAME, SQL_PASSWORD, SQL_DBNAME);
	if ($db->connect_error) {
		http_response_code(500);
		die($db->connect_error);
	}

	$stmt = $db->prepare('select x, y, color, round(unix_timestamp(now(3)) * 1000) as now from events join users on (events.user = users.token) where timestamp > from_unixtime(? / 1000) and game = ' . GAME_ID .' order by timestamp');
	if ($stmt === false) {
		http_response_code(500);
		die("Event fetch query preparation failed: " . $db->error);
	}
	$stmt->bind_param('s', $since);
	if (!$stmt->execute()) {
		$db->rollback();
		http_response_code(500);
		die("Event fetch failed");
	}

	$cur = $stmt->get_result();
	$stmt->close();

	$now = null;
	$changes = [];
	while ($c = $cur->fetch_assoc()) {
		$now = $c['now'];
		unset($c['now']);
		$changes[] = $c;
	}
	
	$cur->free();

	$db->close();

	header('Content-Type: application/json');
	echo json_encode(['timestamp' => $now, 'changes' => $changes]);
}

?>
