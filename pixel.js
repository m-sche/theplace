class PixelAPI {
	constructor() {
		this.timestamp = null;
		this.server = 'pixel.php';
	}

	async putpixel(x, y, color) {
		let body = new FormData()
		body.append('x', x);
		body.append('y', y);
		body.append('color', color);

		let resp = await fetch(this.server, {
			method: 'POST',
			body: body
		});

		if (resp.status !== 200)
			throw Error(await resp.text());
	}

	async get_board() {
		let resp = await fetch(this.server, {
			method: 'GET'
		});

		if (resp.status !== 200)
			throw Error(await resp.text());

		let json = await resp.json();

		this.timestamp = json.timestamp;
		delete json.timestamp;

		return json;
	}

	async get_changes(since) {
		if (since === undefined)
			since = this.timestamp

		let resp = await fetch(this.server + '?since=' + since, {
			method: 'GET',
		});

		if (resp.status !== 200)
			throw Error(resp.text());

		let json = await resp.json();

		if (json.timestamp)
			this.timestamp = json.timestamp;
		delete json.timestamp;

		return json.changes;
	}
}
