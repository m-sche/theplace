<!DOCTYPE html>
<html>
<head>
<meta charset='utf-8'>
<title>The place</title>
</head>
<body>

<div id="top"><div id="buttons"></div><div id="timer"></div></div>
<main><canvas id="canvas"></canvas></main>
<div id="error"></div>

<style>
body {
	margin: 0;
	width: 100%;
	background: lightgray;
}

main {
	text-align: center;
}

canvas {
	height: 90vh;
	display: inline-block;
	image-rendering:crisp-edges;
	margin-top: 1vh;
	margin-left: auto;
	margin-right: auto;
	box-shadow: 0 0 10px black;
}

button {
	width: 5vh;
	height: 5vh;
	border: 2px solid darkgray;
	padding: 0;
}

button.selected {
	border: 4px dashed lightgray;
}

#buttons {
	flex: 1 1 100%;
}

#top {
	height: 5vh;
	padding: 10px;
	display: flex;
}

#timer {
	font-size: 2em;
	font-family: monospace;
	text-align: right;
	line-height: 1.6em;
	color: red;
	text-shadow: 0.1vw 0.1vw black;

	position: fixed;
	font-size: 4vw;
	bottom: 0px;
	left: 1vw;
}

#timer.zero {
	color: #5bff5b;
}

#error {
	background: #ffd2d2;
	border: 2px solid #ff5b5b;
	position: absolute;
	top: 10px;
	right: 6em;
	padding: 10px;
	display: none;
	color: #ff5b5b;
	font-size: 1.2em;
}
</style>

<script src="pixel.js"></script>

<script>
var api = new PixelAPI();

const thick = 1;
const pix = 22;

const wait = 10 * 1000;	// How often to pull changes [ms]

const anim_step = 50;		// How long to wait between animation step [ms]
const anim_atonce = 4;		// How many pixels to draw at once
const anim_end = 60 * 1000;	// How often to show completed animation [ms]

var selected = -1;	// Selected color code
let highlighted = null; // Highlighted coordinates {x, y}

var context;

var button = {};

let board;

init();

let errorTimer = null;
function displayError(err)
{
	console.log(err)

	let errorbox = document.getElementById('error');
	errorbox.innerText = err;
	errorbox.style.display = 'initial';

	clearTimeout(errorTimer);
	errorTimer = setTimeout(() => {
		errorbox.style.display = 'none';
	}, 3000)
}

function clearHighlight()
{
	if (highlighted)
		plot(highlighted.x, highlighted.y, board.board[highlighted.y * board.dimensions[0] +  highlighted.x]);
	highlighted = null;
}

function relative_xy(event)
{
	var rel_x;
	var rel_y;
	if (event.offsetX) {
		rel_x = event.offsetX;
		rel_y = event.offsetY;
	} else {
		var target = event.target;
		var rect = target.getBoundingClientRect();

		rel_x = event.clientX - rect.left;
		rel_y = event.clientY - rect.top;
	}

	var x = Math.round(rel_x * board.dimensions[0] / canvas.clientWidth - 0.5);
	var y = Math.round(rel_y * board.dimensions[1] / canvas.clientHeight - 0.5);

	return [x, y]
}

async function init()
{
	var anim = 0;
	<?php
		if (isset($_GET["anim"])) {
			echo "anim = 1;";
		}
	?>

	try {
		board = await api.get_board();
	} catch (err) {
		displayError(err);
	}

	var size_x = board.dimensions[0];
	var size_y = board.dimensions[1];

	var canvas = document.getElementById("canvas");
	if (!canvas.getContext) {
		alert("Please use a Canvas-Supporting Web Browser");
		return;
	}
	context = canvas.getContext('2d');

	context.canvas.width = size_x * pix;
	context.canvas.height = size_y * pix;

	if (anim == 1) {
		animate();
		return;
	}

	let timer = new Timer('timer', board.cooldown, board.remaining);

	context.fillStyle = "lightgray";
	context.fillRect(0, 0, size_x * pix, size_y * pix);
	
	bar();

	var i = 0;
	for (var y = 0; y < size_y; y++) {
		for (var x = 0; x < size_x; x++) {
			plot(x, y, board.board[i]);
			i++;
		}
	}

	async function on_click(event)
	{
		if (selected == -1)
			return;

		[x, y] = relative_xy(event);

		try {
			if (!timer.isready) {
				clearHighlight();
				throw Error('You must wait for cooldown');
			}
			await api.putpixel(x, y, selected);
			plot(x, y, selected);
			board.board[y * board.dimensions[0] + x] = selected;
			timer.reset();
		} catch (err) {
			displayError(err);
		}
	}

	function on_mousemove(event)
	{
		clearHighlight();

		[x, y] = relative_xy(event);

		plot(x, y, selected)
		highlighted = {x, y}
	}

	// Disable the following at the end of the game
	canvas.addEventListener('click', on_click);
	canvas.addEventListener('mousemove', on_mousemove);
	canvas.addEventListener('mouseout', clearHighlight);
	setInterval(update, wait);
}

// Color may be a color code (according to board.colors) or a CSS color ('red' or '#ff0055')
function plot(x, y, color, border = true)
{
	if (isNaN(parseInt(color)))
		context.fillStyle = color;
	else
		context.fillStyle = '#' + board.colors[color];
	
	if (border) {
		context.fillRect(
			(x * pix) + thick, (y * pix) + thick,
			pix - 2 * thick, pix - 2 * thick);
	} else {
		context.fillRect(x * pix, y * pix, pix, pix);
	}
}

async function update()
{
	try {
		var changes = await api.get_changes();
	} catch (err) {
		displayError(err);
	}

	for (change of changes) {
		if (!(change.color in board.colors))
			window.location = window.location;	// Reload page

		board.board[change.y * board.dimensions[0] + change.x] = change.color;
		plot(change.x, change.y, change.color);
	}
}

function delay(ms)
{
	return new Promise((resolve, reject) => {
		setTimeout(resolve, ms)
	})
}

async function animate()
{
	try {
		var changes = await api.get_changes(0);
	} catch (err) {
		displayError(err);
	}

	while (true) {
		context.fillStyle = '#' + board.colors[board.background];
		context.fillRect(0, 0, board.dimensions[0] * pix, board.dimensions[1] * pix);

		// Clear the logical board
		for (let i = 0; i < board.dimensions[0] * board.dimensions[1]; i++)
			board.board[i] = board.background;

		let i = 0;
		for (let change of changes) {
			if (!(change.color in board.colors))
				window.location = window.location;	// Reload page

			if (board.board[change.y * board.dimensions[0] + change.x] != change.color) {
				i++;
				board.board[change.y * board.dimensions[0] + change.x] = change.color;
				plot(change.x, change.y, change.color, false);

				if (i % anim_atonce == 0)
					await delay(anim_step);
			}
		}
		await delay(anim_end);
	}
}

function bar()
{
	var container = document.getElementById("buttons");

	function on_click(event)
	{
		if (selected != -1)
			button[selected].classList.remove('selected');
		selected = event.target.id;
		event.target.classList.add('selected');
	}

	for (key in board.colors) {
		var btn = document.createElement('button');
		btn.style.background = '#' + board.colors[key];
		btn.id = key;
		btn.addEventListener('click', on_click);

		if (selected == -1) {
			selected = key;
			btn.classList.add('selected');
		}

		button[key] = btn;
		container.appendChild(btn);
	}
}

class Timer {
	constructor(id, cooldown, init = 0) {
		this.element = document.getElementById(id);
		this.cd = cooldown;
		this.remaining = init;

		// Actually wait 1s moreto be sure
		setInterval(this._decrease.bind(this), 1000 + 1000 / this.cd);
		this._render();
	}
	
	_decrease() {
		if (this.remaining) {
			this.remaining--;
			this._render();
		}
	}

	_render() {
		this.element.innerText = String(Math.floor(this.remaining / 60)).padStart(2, '0') + ':' + String(this.remaining % 60).padStart(2, '0');
		if (this.isready)
			this.element.classList.add('zero');
	}

	reset() {
		this.remaining = this.cd;
		this._render();
		this.element.classList.remove('zero');
	}

	get isready() {
		return !this.remaining;
	}
}

</script>
</body>
</html>
