class PixelAPI {
	constructor() {
		this.dim = [0, 0]
		this.colors = {}
	}

	async putpixel(x, y, color) {
		return Promise.resolve()
	}

	async get_board() {
		return new Promise((resolve, reject) => {
			const colors = ['ffffff', '000000', 'ff5b5b', 'ff5bd2', 'd25bff', '5b5bff', '5bd2ff', '5bffd2', '5bff5b', 'd2ff5b', 'ffff5b', 'ffd25b']
			let c = {}

			let ix = 0
			for (let i in colors) {
				if (Math.random() > 0.4)
					c[String(i)] = colors[i]
					
			}

			this.dim = [Math.floor(Math.random() * 10) * 10 + 50, Math.floor(Math.random() * 5) * 10 + 50];
			let board = [];

			for (let i = 0; i < this.dim[0] * this.dim[1]; i++)
				board.push(Object.keys(c)[Math.floor(Math.random() * Object.keys(c).length)]);

			this.colors = c;

			resolve({
				colors: this.colors,
				cooldown: Math.floor(Math.random() * 10 + 5),
				dimensions: this.dim,
				board: board
			});
		});
	}

	async get_changes() {
		return new Promise((resolve, reject) => {
			let ch = []
			while (Math.random() > 0.3 && ch.length < 5) {
				ch.push({
					x: Math.floor(Math.random() * this.dim[0]),
					y: Math.floor(Math.random() * this.dim[1]),
					color: Object.keys(this.colors)[Math.floor(Math.random() * Object.keys(this.colors).length)],
				})
			}

			resolve(ch);
		});
	}
}
